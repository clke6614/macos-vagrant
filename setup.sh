
echo "   _____           _    ____   _____   ______ ";
echo "  / ____|         | |  / __ \ / ____| |____  |";
echo " | |     ___ _ __ | |_| |  | | (___       / / ";
echo " | |    / _ \ '_ \| __| |  | |\___ \     / /  ";
echo " | |___|  __/ | | | |_| |__| |____) |   / /   ";
echo "  \_____\___|_| |_|\__|\____/|_____/   /_/    ";
echo "                                              ";
echo "                                              ";


yum update -y

echo 'Install git .......'
yum remove -y git
yum install -y epel-release
yum install -y https://repo.ius.io/ius-release-el7.rpm
yum install -y git224

echo 'Install Docker.......'
# yum-utils是yum套件管理工具

yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

# 安裝docker與其必要套件
cd /tmp
curl -kO https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-17.12.1.ce-1.el7.centos.x86_64.rpm
curl -kO http://ftp.riken.jp/Linux/cern/centos/7/extras/x86_64/Packages/container-selinux-2.74-1.el7.noarch.rpm

yum install -y docker-ce-17.12.1.ce-1.el7.centos.x86_64.rpm container-selinux-2.74-1.el7.noarch.rpm

# Setting Docker daemon
echo 'Setting Docker daemon.......'
mkdir /etc/docker
cat <<EOF > /etc/docker/daemon.json
{
    "dns": ["10.68.72.200", "8.8.8.8"],
    "insecure-registries":["registry-fortress.fareastone.com.tw"]
}
EOF

mkdir /etc/systemd/system/docker.service.d
cat <<EOF > /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
EnvironmentFile=-/etc/sysconfig/docker
EOF

systemctl daemon-reload
systemctl start docker
systemctl enable docker
usermod -aG docker vagrant
docker info

# 安裝docker-compose
echo 'Install docker-compose....'
curl -k -s -L https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "  ______ _       _     _     ";
echo " |  ____(_)     (_)   | |    ";
echo " | |__   _ _ __  _ ___| |__  ";
echo " |  __| | | '_ \| / __| '_ \ ";
echo " | |    | | | | | \__ \ | | |";
echo " |_|    |_|_| |_|_|___/_| |_|";
echo "                             ";
echo "                             ";
