# VMSetup

The project is still in its early stages.

Setup a CentOS 7 VM environment for development in FET. By using a shell script to setup all necessary config, software.

# Install vagrnat in windows

Run following script in cmd.

```sh
# Install chocolatey https://chocolatey.org
# The package manager for Windows
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# Install vagrant
choco install vagrant -y

# Install virtualbox if not installed
choco install virtualbox -y

# Upgrade Powershell to newer version
# Required by vagrant
choco install powershell -y

# Set system wide proxy
setx /m http_proxy http://fetfw.fareastone.com.tw:8080
setx /m https_proxy http://fetfw.fareastone.com.tw:8080

# Reboot your computer
```

# Install Vagrant Plugin

```sh
# Install vagrant plugin for vm proxy setting
vagrant plugin install vagrant-proxyconf --plugin-clean-sources --plugin-source http://rubygems.org

# Install virtualbox guest plugin. Share folder feature in virtualbox need vbguest to work.
# Virtualbox guest which is not pre-installed in centos7 in vagrant box
vagrant plugin install vagrant-vbguest --plugin-clean-sources --plugin-source http://rubygems.org
```

# Start

## Clone Project

```sh
# change directory to D partition
D:

# clone project or download zip file extract to D partition
git clone ssh://git@fortress.fareastone.com.tw:2022/itt-iaps/VMSetup_CentOS.git
cd VMSetup_CentOS
```

## Sync Development Folder

Edit `Vagrantfile` to mount folder in the guest machine.
Replace this line in Vagrantfile
```ruby
# config.vm.synced_folder "../workspace", "/workspace"
```

For example, given the following configuration:
```ruby
config.vm.synced_folder "../workspace", "/workspace"
```
The `workspace` folder in parent folder will be mounted to `/workspace` in the vm.

[Basic Usage - Synced Folders - Vagrant by HashiCorp](https://www.vagrantup.com/docs/synced-folders/basic_usage.html)

## Start VM

```sh
# Create a vm and setup environment for dev
# Need more than 10min
vagrant up

# Connect into vm via ssh
vagrant ssh
```

## Setup SSH key for git (Optional)

Skip this step if you use Git on Windows.

```sh
# Generate ssh key for git
cd ~/.ssh
ssh-keygen -f id_rsa -t rsa -N ''

# Copy output to gitlab
# Example Output:
#     ssh-rsa AAAAB3NzaC1y....P7yyC0epl vagrant@localhost.localdomain
# Steps in Gitlab: Setting > SSH Keys > Paste output in Key input field > Add Key
cat ~/.ssh/id_rsa.pub

# Set who you are
git config --global user.name "John Doe"
git config --global user.email johndoe@fareastone.com.tw

# Convert CRLF to LF on commit
git config --global core.autocrlf input

# Set default text editor to vim
git config --global core.editor vim
```

## Login gitlab registry

```sh
# Login with your gitlab account
docker login registry-fortress.fareastone.com.tw
```

## VM snapshot

VM takes long time to install software and require set lots of setting. So we can use snapshot to keep the working version of VM. The following command will help us to do snapshot. `vagrant snapshot save <name>` can save the current state of VM in this folder. You need to specify a name to a snapshot. `vagrant snapshot restore --no-provision <name>` can restore name of the saved snapshot VM.

Here are an example to save a snapshot called baby.

```sh
# Save the VM
vagrant snapshot save baby

# Restore command
vagrant snapshot restore --no-provision baby
```

# Useful Vagrant Command

| Command | Description |
|---------|--------------|
| vagrant up      | starts and provisions the vagrant  |
| vagrant ssh     | connects to machine via SSH |
| vagrant suspend | suspends the machine environment |
| vagrant halt    | stops the vagrant machine |
| vagrant reload  | restarts vagrant machine, loads new Vagrantfile configuration |
| vagrant destroy | stops and deletes all traces of the vagrant machine |

# Troubleshooting

## My config does not apply to the vm
Everytime you modify Vagrantfile, you need to execute `vagrant reload` to update config to vm.

## Centos official changelog
[Updated CentOS Vagrant Images Available (v1804.02) – Blog.CentOS.org](https://blog.centos.org/2018/05/updated-centos-vagrant-images-available-v1804-02/)